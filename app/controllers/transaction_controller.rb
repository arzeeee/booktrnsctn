class TransactionController < ApplicationController

  def index

  end

  def show

  end

  def return_book
    id = params[:transaction][:transaction_id]
    Transaction.find(id).update(returned: Date.today)
    redirect_to transaction_list_path

  end

  def list
    filter = check_filter()
    if filter
      @transaction = filter
    else
      if params['search'].nil?
        @transaction = Transaction.all
      else
        @transaction =
          Transaction
          .joins(:book)
          .joins(:user).
          where('title like ? OR name like ?', "%#{params['search']}%",  "%#{params['search']}%")
      end
    end
  end

  def create
    @transaction = Transaction.new(transaction_params)
    @transaction.pinjam = Date.new(borrow_date[0], borrow_date[1], borrow_date[2])
    @transaction.kembali = Date.new(return_date[0], return_date[1], return_date[2])
      if @transaction.kembali <= @transaction.pinjam
      flash[:error] = "Error. Tanggal kembali harus > tanggal peminjaman"
      redirect_to transaction_borrow_path
    else
      if @transaction.save
        flash[:info] = "Peminjaman tercatat"
      else
        flash[:error] = "Error. Peminjaman tidak tercatat"
      end
      redirect_to transaction_borrow_path
    end
  end

  def borrow
    @user = User.all
    @book = Book.all
    @transaction = Transaction.new
  end

  private
  def transaction_params
    params.require(:transaction).permit(:user_id, :book_id, :pinjam, :kembali)
  end

  def borrow_date
    [
      params.require(:pinjam)['pinjam(1i)'].to_i,
      params.require(:pinjam)['pinjam(2i)'].to_i,
      params.require(:pinjam)['pinjam(3i)'].to_i
    ]
  end

  def return_date
    [
      params.require(:kembali)['kembali(1i)'].to_i,
      params.require(:kembali)['kembali(2i)'].to_i,
      params.require(:kembali)['kembali(3i)'].to_i
    ]
  end

  def check_filter
    if params[:filter] == "late"
      Transaction.where("returned > kembali")
    elsif params[:filter] == "unreturned"
      Transaction.where('returned is null')
    else
      nil
    end
  end

end
