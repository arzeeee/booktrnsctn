class Transaction < ApplicationRecord
  attr_accessor :borrow_date, :return_date
  attr_accessor :search

  belongs_to :user
  belongs_to :book

end
