# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(name: "name_1")
User.create(name: "name_2")
User.create(name: "name_3")

Book.create(title: "Title 1", genre: "Horror", available: 3)
Transaction.create(
  user: User.first,
  book: Book.first,
  pinjam: 2.month.ago,
  kembali: 1.month.ago,
  returned: 1.day.ago
)

Transaction.create(
  user: User.first,
  book: Book.first,
  pinjam: 2.month.ago,
  kembali: 1.month.ago,
)
