class AddReturnedToTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :transactions, :returned, :date
  end
end
