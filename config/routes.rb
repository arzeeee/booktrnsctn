Rails.application.routes.draw do
  root "pages#home"
  
  resources :books
  resources :users
  resources :transaction, except: :show
  namespace :transaction do
    get "borrow"
    get "list"
    get "return_book"
  end
  # get "borrow", to: "transaction#borrow"

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
